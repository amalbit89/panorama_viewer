import copy
import numpy as np 
import cv2
from pan_crop import crop_panorama_image

distance_threshold = 600
split_offset = 200
offset = 30

center_offset = 300
x_threshold = 20
cord_x_diff_threshold = 550

line_visible = True


def get_fov_x(x_1, x_2, width):
    x1_angle = x_1/width * 360
    x2_angle = x_2/width * 360
    fov = x2_angle - x1_angle
    return fov


def get_angle_y(pix_y, height):
    y_angle = (pix_y - height / 2)/height * 180
    return y_angle


def crop_normal(pan_image, all_corners):
    all_corners = copy.deepcopy(all_corners)
    all_corners_x_sorted = sorted(all_corners, key=lambda k: k[0])
    cord_x_min = np.array(all_corners_x_sorted[0])
    cord_x_max = np.array(all_corners_x_sorted[-1])

    cord_x_min_off = (cord_x_min[0] - offset, cord_x_min[1])
    cord_x_max_off = (cord_x_max[0] + offset, cord_x_max[1])

    cord_x_mid = int((cord_x_min_off[0]+cord_x_max_off[0])/2)
    cord_x_diff = int(abs(cord_x_min_off[0]+cord_x_max_off[0]))
    half_pan_width = int(pan_image.shape[1]/2)
    if cord_x_mid == half_pan_width:
        cord_angle = 0
    elif cord_x_mid < half_pan_width:
        cord_angle = -int((half_pan_width - cord_x_mid) * 180 / half_pan_width)
    else:
        cord_angle = int((cord_x_mid-half_pan_width) * 180 / half_pan_width)
    cord_angle += 180

    print('min x :', cord_x_min_off[0], 'max x :', cord_x_max_off[0], 'diff x :',cord_x_min_off[0]- cord_x_max_off[0], 'mmid x :', cord_x_mid, 'angle :', cord_angle, "size=", pan_image.shape)

    fov = get_fov_x(cord_x_min_off[0], cord_x_max_off[0], pan_image.shape[1])
    print('fov :' ,fov)

    if line_visible:
        cv2.line(pan_image, (cord_x_min_off[0], 0), (cord_x_min_off[0], 500), (255, 0, 0), 3)
        cv2.line(pan_image, (cord_x_max_off[0], 0), (cord_x_max_off[0], 500), (255, 0, 0), 3)
        cv2.line(pan_image, (cord_x_mid, 0), (cord_x_mid, 500), (0, 255, 0), 3)
        cv2.line(pan_image, (500, 0), (500, 500), (0, 0, 255), 3)
        cv2.imshow('line', pan_image)
        cv2.waitKey(0)

    width = fov / 360 * pan_image.shape[1]
    
    # 342 = where to look at y axis
    phi = get_angle_y(342, pan_image.shape[0])
    print("height = ", width)

    # look at up and down , give value for phi
    #crop_pan = crop_panorama_image(pan_image, theta=cord_angle, phi=phi, res_x=500, res_y=int(width), fov=fov, debug=False)

    if cord_x_diff < cord_x_diff_threshold:
        crop_pan = crop_panorama_image(pan_image, theta=cord_angle, phi=0.0, res_x=int(width), res_y=500, fov=fov)
    else:
        crop_pan = pan_image[:, cord_x_min_off[0]:cord_x_max_off[0]]

    return crop_pan