### Panarama Viewer ###

The selected area of the panarama image is straighted and cropped.


```
    x    -180 -90 0 90 180

    -
    y 0 
    +

```
     
#1. Four panaroma coordinates are required (as rectangle). 
From these four coordintes, the min and max x values are selected as the required area of panarama.(two vertical lines)
From these lines , the theta angle is calculated to roate the panarama. (Please add 180 to that angle)

```
   cord_x_mid = int((cord_x_min_off[0]+cord_x_max_off[0])/2)
    cord_x_diff = int(abs(cord_x_min_off[0]+cord_x_max_off[0]))
    half_pan_width = int(pan_image.shape[1]/2)
    if cord_x_mid == half_pan_width:
        cord_angle = 0
    elif cord_x_mid < half_pan_width:
        cord_angle = -int((half_pan_width - cord_x_mid) * 180 / half_pan_width)
    else:
        cord_angle = int((cord_x_mid-half_pan_width) * 180 / half_pan_width)
    cord_angle += 180
```
    
#2. Calculate the Field of View (FOV) to see up and down of the panarama.
For that, the point of panarama is required to identify where looking at.

```
     fov = get_fov_x(cord_x_min_off[0], cord_x_max_off[0], pan_image.shape[1]) 
     width = fov / 360 * pan_image.shape[1]
     #342 = where to look at y axis
     phi = get_angle_y(342, pan_image.shape[0])
```
 
#3. If no need to see up and down then phi = 0.0 
 ```
     crop_pan = crop_panorama_image(pan_image, theta=cord_angle, phi=0.0, res_x=int(width), res_y=500, fov=fov)
 ```
 
#4. If need to see up or down
```
    crop_pan = crop_panorama_image(pan_image, theta=cord_angle, phi=phi, res_x=int(width), res_y=500, fov=fov)
```
